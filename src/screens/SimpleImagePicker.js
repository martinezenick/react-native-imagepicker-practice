import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Alert } from 'react-native';
import { STYLES, COLORS } from './Styles';
import * as ImagePicker from 'react-native-image-picker';
import {launchImageLibrary} from 'react-native-image-picker';





export default function SimpleImagePicker() {
  const [imageSource, setImageSource] = useState(null);

  const selectImage = () => {
      let options = {
          title: 'Image Library',
          maxWidth: 256,
          maxHieght: 256,
          storageOptions: {
              skipBackup: true
          }
      };

    ImagePicker.launchImageLibrary(options, response => {
      console.log({ response });

      if (response.didCancel) {
          console.log('User canelled photo picker');
          Alert.alert('You did not select an image')
      }
      else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButon) {
          console.log('User tapped custom button: ', response.customButton);
      }
      else {
          let source = { uri:response.uri};
          console.log({ source });
      }
    });
  }

  const takeImage = () => {
    let options = {
      title: 'Camera',
      maxWidth: 256,
      maxHeight: 256,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.launchCamera(options, response => {
      console.log({ response })

      if (response.didCancel) {
        console.log('User canelled photo picker');
        Alert.alert('You did not select an image')
      }
      else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButon) {
          console.log('User tapped custom button: ', response.customButton);
      }
      else {
          let source = { uri:response.uri};
          console.log({ source });
      }

    })
  }

  return (
    <View
      style={[
        STYLES.flex,
        STYLES.centerContainer,
        { backgroundColor: COLORS.primaryDark }
      ]}
    >
      <Text style={[STYLES.title, { color: COLORS.primaryLight }]}>
        This app is to see what I can do with simple image picker.
      </Text>
      <TouchableOpacity
        onPress = {selectImage}
        style={[
            STYLES.selectButtonContainer,
            {backgroundColor: COLORS.primaryLight}
        ]}
        >
            <Text style={STYLES.selectButtonTitle}>Pick an image from your library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress = {takeImage}
        style={[
            STYLES.selectButtonContainer,
            {backgroundColor: COLORS.primaryLight}
        ]}
        >
            <Text style={STYLES.selectButtonTitle}>Take a picture</Text>
      </TouchableOpacity>
    </View>
  );
}